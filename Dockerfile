ARG PROJECT=psql-parti

############################
# STEP 1 build executable binary
############################

FROM golang:1.24.1 AS build

ARG PROJECT

WORKDIR /go/src/${PROJECT}/
COPY . /go/src/${PROJECT}/

# Build binary
RUN CGO_ENABLED=0 GOOS=linux GOAMD64=v1 go build -ldflags="-w -s" -o /go/bin/${PROJECT}

############################
# STEP 2 deploy a smaller image
############################

# distroless static linux/amd64
FROM gcr.io/distroless/static:latest

USER nonroot:nonroot

ARG PROJECT

COPY --from=build /go/bin/${PROJECT} /

ENTRYPOINT ["/psql-parti"]
