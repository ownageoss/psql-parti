GROUP=ownageoss
PROJECT_NAME=psql-parti
CONTAINER_REGISTRY=registry.gitlab.com/$(GROUP)/$(PROJECT_NAME)
PROGRAM_VERSION=25.3.1
.DEFAULT_GOAL=run
MAKEFLAGS += --always-make

run:
	go clean
	go build -o main -race
	ENV=LOCAL LOG_MODE=DEV LOG_LEVEL=DEBUG BUILD_VERSION=LOCAL TIMEMACHINE=0 SECRETSPATH=../secrets/$(PROJECT_NAME) ./main

help:
	go clean
	go build -o main
	SECRETSPATH=../../secrets/$(PROJECT_NAME) ./main -h

clean:
	-go clean

updatedeps:
	go get -u ./...
	go mod tidy

sbom:
	go clean
	bom generate --name gitlab.com/ownageoss/$(PROJECT_NAME) --output=$(PROJECT_NAME).spdx .
	bom document outline $(PROJECT_NAME).spdx

checks:
	gitleaks detect -v --no-git
	golangci-lint run --no-config
	govulncheck ./...
	gosec -tests ./...

brutal:
	golangci-lint run --enable-all