module psql-parti

go 1.24

require (
	github.com/jackc/pgx/v5 v5.7.2
	gitlab.com/ownageoss/logging v0.17.0
	gitlab.com/ownageoss/utils v0.29.0
	go.uber.org/zap v1.27.0
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/juju/ratelimit v1.0.2 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.36.0 // indirect
	golang.org/x/text v0.23.0 // indirect
)
