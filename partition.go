package main

import (
	"context"
	"strings"
)

func (instance *cronJob) createPartition(ctx context.Context) {
	// Get year,month for the next 2 months. This will take care of cross year partitions.
	// Logic built with time machine mode to move forward or backward in time.
	const nextMonthQuery = `
		SELECT
			TO_DATE((
				SELECT
					DATE_TRUNC('month', CURRENT_DATE + $1::int + INTERVAL '1 month')::text), 'YYYY-MM-DD')::text
	`
	const monthAfterNextMonthQuery = `
		SELECT
			TO_DATE((
				SELECT
					DATE_TRUNC('month', CURRENT_DATE + $1::int + INTERVAL '2 month')::text), 'YYYY-MM-DD')::text
	`

	var nextMonth, monthAfterNextMonth string
	err := instance.db.QueryRow(
		ctx,
		nextMonthQuery,
		instance.timeMachine,
	).Scan(
		&nextMonth,
	)
	if err != nil {
		instance.logger.Fatal(err)
	}

	err = instance.db.QueryRow(
		ctx,
		monthAfterNextMonthQuery,
		instance.timeMachine,
	).Scan(
		&monthAfterNextMonth,
	)
	if err != nil {
		instance.logger.Fatal(err)
	}

	// Convert 2020-01-01 to 20200101
	nextMonthTable := strings.ReplaceAll(nextMonth, "-", "")
	// Convert 20200101 to 202001
	nextMonthTable = strings.TrimSuffix(nextMonthTable, "01")

	var partitions []string

	for _, tableName := range instance.TablesToPartition {
		tableName = strings.TrimSpace(tableName)

		// Prepare partition query.
		_, err = instance.db.Prepare(
			ctx,
			"prepareParititionQuery",
			"SELECT FORMAT ('CREATE TABLE IF NOT EXISTS %s PARTITION OF %s FOR VALUES FROM (%s)  TO (%s)',	quote_ident($1), quote_ident($2), quote_literal($3), quote_literal($4))",
		)
		if err != nil {
			instance.logger.Fatal(err)
		}

		partitionName := tableName + "_" + nextMonthTable

		// Create partitions for next month
		var parititionQuery string
		err = instance.db.QueryRow(
			ctx,
			"prepareParititionQuery",
			partitionName,
			tableName,
			nextMonth,
			monthAfterNextMonth,
		).Scan(
			&parititionQuery,
		)
		if err != nil {
			instance.logger.Fatal(err)
		}

		instance.logger.Debug(parititionQuery)

		_, err = instance.db.Exec(
			ctx,
			parititionQuery,
		)
		if err != nil {
			instance.logger.Fatal(err)
		}

		partitions = append(partitions, partitionName)
	}

	if len(instance.UsersToGrant) == 0 {
		return
	}

	for _, user := range instance.UsersToGrant {
		user = strings.TrimSpace(user)
		instance.grantToUser(
			ctx,
			user,
			partitions,
		)
	}
}

func (instance *cronJob) grantToUser(
	ctx context.Context,
	user string,
	partitions []string,
) {
	for _, partition := range partitions {
		// Prepare GRANT query
		_, err := instance.db.Prepare(
			ctx,
			"prepareGrantQuery",
			"SELECT FORMAT ('GRANT SELECT ON TABLE %s TO %s', quote_ident($1), quote_ident($2))",
		)
		if err != nil {
			instance.logger.Fatal(err)
		}

		// GRANT permissions
		var grantQuery string
		err = instance.db.QueryRow(
			ctx,
			"prepareGrantQuery",
			partition,
			user,
		).Scan(
			&grantQuery,
		)
		if err != nil {
			instance.logger.Fatal(err)
		}

		instance.logger.Debug(grantQuery)

		_, err = instance.db.Exec(
			ctx,
			grantQuery,
		)
		if err != nil {
			instance.logger.Fatal(err)
		}
	}
}
