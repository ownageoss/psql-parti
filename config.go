package main

import (
	"errors"
	"os"
	"path"

	"gitlab.com/ownageoss/utils"
)

// loadConfig - load config.
func (instance *cronJob) loadConfig() {
	instance.timeMachine = os.Getenv("TIMEMACHINE")
	if instance.timeMachine == "" {
		instance.timeMachine = "0"
	}

	// Load config
	err := utils.LoadJSON(path.Join(os.Getenv("SECRETSPATH"), "config.json"), instance)
	if err != nil {
		instance.logger.Fatal(err)
	}
}

// checkConfig - make sure all required config is defined.
func (instance *cronJob) checkConfig() {
	if instance.DBURL == "" {
		instance.logger.Fatal(errors.New("DBURL config not provided"))
	}
	if len(instance.TablesToPartition) == 0 {
		instance.logger.Fatal(errors.New("TablesToPartition config not provided"))
	}
}
