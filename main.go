package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/jackc/pgx/v5"
	"gitlab.com/ownageoss/logging"

	"go.uber.org/zap"
)

type cronJob struct {
	db     *pgx.Conn
	logger *zap.SugaredLogger

	DBURL             string
	TablesToPartition []string
	UsersToGrant      []string

	timeMachine string
}

func main() {
	ctx, done := signal.NotifyContext(
		context.Background(),
		os.Interrupt,
		syscall.SIGINT,
		syscall.SIGTERM,
	)

	logger := logging.NewLoggerFromEnv().
		With("build_version", os.Getenv("BUILD_VERSION"))

	ctx = logging.WithLogger(ctx, logger)

	defer func() {
		done()
		if err := recover(); err != nil {
			logger.Fatal(err)
		}
	}()

	err := realMain(ctx)
	done()

	if err != nil {
		logger.Fatal(err)
	}

	logger.Infof("Shutdown successful.")
}

func realMain(ctx context.Context) error {
	start := time.Now()

	instance := &cronJob{}

	instance.logger = logging.FromContext(ctx)

	instance.loadConfig()

	instance.checkConfig()

	instance.initDB(ctx)

	instance.createPartition(ctx)

	totalDurationStr := fmt.Sprintf("Total execution time: %v", time.Since(start))

	instance.logger.Info(totalDurationStr)

	return instance.gracefulShutdown(ctx)
}

// gracefulShutdown closes open connections.
func (instance *cronJob) gracefulShutdown(ctx context.Context) error {
	err := instance.db.Close(ctx)
	if err != nil {
		instance.logger.Errorf("Error closing DB connection: %v", err)
		return err
	}
	instance.logger.Infof("DB connection closed ...")

	return nil
}
