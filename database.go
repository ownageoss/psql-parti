package main

import (
	"context"

	"github.com/jackc/pgx/v5"
)

// initDB - initialize a connection to DB.
func (instance *cronJob) initDB(ctx context.Context) {
	var err error

	// Connect to DB
	instance.db, err = pgx.Connect(ctx, instance.DBURL)
	if err != nil {
		instance.logger.Fatal("error connecting to the database: ", err)
	}

	err = instance.db.Ping(ctx)
	if err != nil {
		instance.logger.Fatal("error pinging database: ", err)
	}

	instance.logger.Info("Connected to DB ...")
}
