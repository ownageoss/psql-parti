# PSQL Parti

PSQL Parti (PostgreSQL Partitioner) is a simple program that creates [time based range partitions](https://www.postgresql.org/docs/current/ddl-partitioning.html#DDL-PARTITIONING-OVERVIEW-RANGE) for the next calendar month for tables specified in the config.

`TIMEMACHINE` environment variable can be used to create partitions for any month in the past/future.

## Sample Use Case

Consider the following sample Postgres DB setup:

```sql
CREATE ROLE user1 WITH ENCRYPTED PASSWORD 'supersecretstring1' LOGIN;

CREATE ROLE readonly1 WITH ENCRYPTED PASSWORD 'supersecretstring2' LOGIN;

CREATE ROLE readonly2 WITH ENCRYPTED PASSWORD 'supersecretstring3' LOGIN;

CREATE DATABASE demo OWNER user1;

\c demo;
SET ROLE user1;

CREATE TABLE IF NOT EXISTS table_1(
  id uuid NOT NULL DEFAULT gen_random_uuid(),
  creation_time timestamptz NOT NULL DEFAULT NOW(),
  PRIMARY KEY (id, creation_time)
)
PARTITION BY RANGE (creation_time);

GRANT SELECT ON table_1 TO readonly1, readonly2;

CREATE TABLE IF NOT EXISTS table_2(
  id uuid NOT NULL DEFAULT gen_random_uuid(),
  creation_time timestamptz NOT NULL DEFAULT NOW(),
  PRIMARY KEY (id, creation_time)
)
PARTITION BY RANGE (creation_time);

GRANT SELECT ON table_2 TO readonly1, readonly2;
```

For the schema above, we would want to create partitions for the current month before any data is inserted. Then, we need a method to automatically create partitions for the following months.

PSQL Parti automatically creates partitions for the next calendar month for tables specified in the config and also grants any configured users the `SELECT` permission.

We can run the program as a cronjob to automatically create partitions for subsequent months.

`TIMEMACHINE` mode can be used to create partitions for the current month, any previous month or any future month.

## Example Config

An example config file is provided for reference.

```json
// config.json
{
    "DBURL": "postgresql://user:supersecretstring@host:port/dbname?sslmode=disable",
    "TablesToPartition": [
        "table_1",
        "table_2"
    ],
    "UsersToGrant": [
        "readonly1",
        "readonly2"
    ]
}
```

## Run Locally

First create a `config.json`

```sh
mkdir -p ~/secrets/psql-parti
cd ~/secrets/psql-parti
touch config.json
```

Download and run binary as follows:

```sh
mkdir -p ~/psql-parti
cd ~/psql-parti

# Download binary for amd64 architecture.
wget "https://gitlab.com/api/v4/projects/57901999/packages/generic/psql-parti/25.1.1/psql-parti-linux-amd64.zip" -O psql-parti-linux-amd64.zip
unzip psql-parti-linux-amd64.zip && mv psql-parti-linux-amd64 psql-parti
chmod +x psql-parti

# Create partition for next month (default).
ENV=PROD LOG_MODE=PROD LOG_LEVEL=INFO BUILD_VERSION=v25.1.1 TIMEMACHINE=0 SECRETSPATH=/path/to/secrets/ psql-parti
```

## Run as a Cron Job

It is best to run this as a cron job to automate creation of partitions.

Configure the crontab entry so that the script runs at least 3 times in any given month. This is to maximize the chance of creating a partition in the event there are outages.

```sh
# Example crontab entry that runs on the 5th, 15th and 25th of every month at 5:30 am.
# Tweak variables as needed.
30 5 5,15,25 * *       ENV=PROD LOG_MODE=PROD LOG_LEVEL=INFO BUILD_VERSION=v25.1.1 TIMEMACHINE=0 SECRETSPATH=/path/to/secrets/ /path/to/psql-parti
```

## Run on K8S

Refer to the samples in the [k8s](k8s) folder to run the program as a `Cronjob` on K8S.

## Timemachine

To help detect boundaries of the time based range partition for the following month, we add a postgres `interval` of 1 month to the `current_date`.

Using the `TIMEMACHINE` environment variable, we can move the `current_date` backward of forward to create partitions for any month.

```sh
# Create partition for next month (default).
ENV=PROD LOG_MODE=PROD LOG_LEVEL=INFO BUILD_VERSION=v25.1.1 TIMEMACHINE=0 SECRETSPATH=/path/to/secrets/ /path/to/psql-parti

# Create partition for month after next month.
ENV=PROD LOG_MODE=PROD LOG_LEVEL=INFO BUILD_VERSION=v25.1.1 TIMEMACHINE=30 SECRETSPATH=/path/to/secrets/ /path/to/psql-parti

# Create partition for current month.
ENV=PROD LOG_MODE=PROD LOG_LEVEL=INFO BUILD_VERSION=v25.1.1 TIMEMACHINE=-30 SECRETSPATH=/path/to/secrets/ /path/to/psql-parti

# Create partition for previous month.
ENV=PROD LOG_MODE=PROD LOG_LEVEL=INFO BUILD_VERSION=v25.1.1 TIMEMACHINE=-60 SECRETSPATH=/path/to/secrets/ /path/to/psql-parti
```

## Gotchas

The following features are not yet supported:

- Partition an existing table that already has data.
- Grant privileges other than `SELECT`.

## Other tools similar to PSQL Parti

- [PostgreSQL Partition Manager](https://github.com/pgpartman/pg_partman)

## Reporting bugs and other issues

Please raise an issue if you spot a bug or a vulnerability.

## Software Bill of Materials (SBOM)

```
               _      
 ___ _ __   __| |_  __
/ __| '_ \ / _` \ \/ /
\__ \ |_) | (_| |>  < 
|___/ .__/ \__,_/_/\_\
    |_|               

 📂 SPDX Document gitlab.com/ownageoss/psql-parti
  │ 
  │ 📦 DESCRIBES 1 Packages
  │ 
  ├ psql-parti
  │  │ 🔗 24 Relationships
  │  ├ CONTAINS FILE Dockerfile (Dockerfile)
  │  ├ CONTAINS FILE Makefile (Makefile)
  │  ├ CONTAINS FILE .gitignore (.gitignore)
  │  ├ CONTAINS FILE .gitlab-ci.yml (.gitlab-ci.yml)
  │  ├ CONTAINS FILE config.go (config.go)
  │  ├ CONTAINS FILE README.md (README.md)
  │  ├ CONTAINS FILE LICENSE (LICENSE)
  │  ├ CONTAINS FILE database.go (database.go)
  │  ├ CONTAINS FILE go.mod (go.mod)
  │  ├ CONTAINS FILE go.sum (go.sum)
  │  ├ CONTAINS FILE k8s/sample-cronjob.yaml (k8s/sample-cronjob.yaml)
  │  ├ CONTAINS FILE k8s/sample-secret.yaml (k8s/sample-secret.yaml)
  │  ├ CONTAINS FILE partition.go (partition.go)
  │  ├ CONTAINS FILE main.go (main.go)
  │  ├ CONTAINS FILE psql-parti.png (psql-parti.png)
  │  ├ CONTAINS FILE psql-parti.spdx (psql-parti.spdx)
  │  ├ DEPENDS_ON PACKAGE go.uber.org/zap@v1.27.0
  │  ├ DEPENDS_ON PACKAGE github.com/juju/ratelimit@v1.0.2
  │  ├ DEPENDS_ON PACKAGE github.com/jackc/pgx/v5@v5.7.2
  │  ├ DEPENDS_ON PACKAGE github.com/jackc/pgpassfile@v1.0.0
  │  ├ DEPENDS_ON PACKAGE github.com/jackc/pgservicefile@v0.0.0-20240606120523-5a60cdf6a761
  │  ├ DEPENDS_ON PACKAGE golang.org/x/crypto@v0.36.0
  │  ├ DEPENDS_ON PACKAGE golang.org/x/text@v0.23.0
  │  └ DEPENDS_ON PACKAGE go.uber.org/multierr@v1.11.0
  │ 
  └ 📄 DESCRIBES 0 Files
```